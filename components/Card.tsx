import styles from "../styles/Cards.module.scss";
import {ICard} from "../interface";
import Link from "next/link";

export const Card = ({id, number, owner}: ICard) => {
    return(
        <div className={styles.card}>
            <Link href={`/cards/${id}`}>
                <a className={styles.card__content}>
                    <span className={styles.card__name}>
                        {owner}
                    </span>
                    <span className={styles.card__number}>
                        {number}
                    </span>
                </a>
            </Link>
        </div>
    )
}
