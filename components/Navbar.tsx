import ActiveLink from "./ActiveLink";

export const Navbar = () => {
    return (
        <header className='header'>
            <nav className='container nav'>
                <ActiveLink href='/' activeClassName='active'>
                    <a className='nav__link'>Home</a>
                </ActiveLink>
                <ActiveLink href="/cards" activeClassName='active'>
                    <a className='nav__link'>Cards</a>
                </ActiveLink>
                <ActiveLink href="/cards/add" activeClassName='active'>
                    <a className='nav__link'>Add</a>
                </ActiveLink>
            </nav>
        </header>
    )
}
