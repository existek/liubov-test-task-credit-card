export interface ICard {
    id?: number,
    number: string,
    owner: string,
    expiryDate: string,
    cvv: number,
    type: string,
    nickname?: string,
}
