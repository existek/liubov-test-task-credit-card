/** @type {import('next').NextConfig} */
require('dotenv').config()
module.exports = {
  reactStrictMode: false,
  env: {
    API_URL: process.env.API_URL
  }
}
