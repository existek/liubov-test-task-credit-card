export const maskCardNumber = (number: string): string => `${number.slice(0, 4)} **** **** ${number.slice(-4)}`;
