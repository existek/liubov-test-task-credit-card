interface IValidationRules {
    [key: string]: (value?: any, msg?: string) => {value: any, message: string}
}
export const validationRules: IValidationRules = {
    required: (value = true, message = 'required field') => ({value, message}),
    pattern: (value: RegExp, msg: string = '') => ({value, message: msg}),
}
export interface IRegExpArr {
    key: string,
    value: RegExp
}
export const regExpArr: IRegExpArr[] = [
    {key: 'Visa', value: /^4[0-9]{12}(?:[0-9]{3})?$/},
    {key: 'Mastercard', value:  /^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/},
];
