import styles from '../styles/Home.module.scss'
import {PageLayout} from "../components/PageLayout";

const Home = () => {
  return (
      <PageLayout title='Home page'>
          <main className={styles.main}>
            <h1 className={styles.title}>
              Welcome to Home page
            </h1>
          </main>
      </PageLayout>
  )
}

export default Home
