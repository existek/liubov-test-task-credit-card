import styles from '../../styles/Card.module.scss';
import {useRouter} from "next/router"
import {useEffect, useState} from "react";
import {PageLayout} from "../../components/PageLayout";
import {ICard} from "../../interface";
import {maskCardNumber} from "../../helpers";

const CardPage = () => {
    const router = useRouter();
    const [loading, setLoading] = useState<boolean>(false);
    const [card, setCard] = useState<ICard>()
    const [error, setError] = useState<string>()
    useEffect(() => {
        const get = async () => {
            try {
                setLoading(true);
                const response = await fetch(`${process.env.API_URL}/cards/${router.query.id}`);
                const json = await response.json();
                if (json) {
                    setCard(json)
                }
            } catch (e) {
                setError('Something went wrong');
            } finally {
                setLoading(false)
            }
        }
        get();
    }, [])
    if (loading) return <div>Loading...</div>
    return(
        <PageLayout title='Card page'>
            <h2 className='page__title'>Card info</h2>
            <div className="row justify-content-center">
                {
                    error && <div className="alert alert-danger">{error}</div>
                }
                {
                    card &&
                    <div className={styles.card}>
                        <div>
                            <div className={styles.card__type}>{card.type}</div>
                            {
                                card.nickname && <div className={styles.card__nickname}>{card.nickname}</div>
                            }
                        </div>
                        <div>
                            <div className={styles.card__number}>{maskCardNumber(card.number)}</div>
                            <div className={styles.card__date}>{card.expiryDate}</div>
                        </div>
                        <div className={styles.card__owner}>{card.owner}</div>
                    </div>
                }

            </div>
        </PageLayout>
    )
}
export default CardPage;
