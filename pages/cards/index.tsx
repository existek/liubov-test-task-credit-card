import styles from '../../styles/Cards.module.scss';
import {ICard} from "../../interface";
import {Card} from "../../components/Card";
import {PageLayout} from "../../components/PageLayout";
import {maskCardNumber} from "../../helpers";

interface IProps {
    cards: ICard[]
}
const CardsPage = ({cards}: IProps) => {
    return (
        <PageLayout title='Cards page'>
            <h2 className='page__title'>Cards page</h2>
            <div className='row justify-content-center'>
                <div className="col-md-8 col-sm-10">
                    <div className={styles.card__list}>
                        {cards.length === 0
                            ?
                            <div className='text-center'>No cards</div>
                            :
                            cards.map((card) => (
                                <Card key={card.id} {...card} />
                            ))
                        }
                    </div>
                </div>
            </div>
        </PageLayout>
    )
}

export const getStaticProps = async () => {
    const response = await fetch(`${process.env.API_URL}/cards`);
    const cards:ICard[] = await response.json()
    if (!cards) {
        return {
            notFound: true
        }
    }
    return {
        props: { cards: cards.map( c => ({...c, number: maskCardNumber(c.number)}) )},
    }
}

export default CardsPage;
