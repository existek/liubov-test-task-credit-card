import {useForm, SubmitHandler, Controller, ControllerRenderProps} from "react-hook-form";
import {useRouter} from "next/router"
import InputMask, {Props as IMaskProps} from 'react-input-mask';
import {PageLayout} from "../../components/PageLayout";
import {ICard} from "../../interface";
import {ChangeEvent, ReactElement, useState} from "react";
import {regExpArr, validationRules} from "../../helpers/constants";

interface IFormInputs {
    number: string,
    owner: string,
    expiryDate: string,
    cvv: number,
    type: string,
    nickname: string,
}

const AddCardPage = () => {
    const router = useRouter();
    const [loading, setLoading] = useState<boolean>(false)
    const [error, handleError] = useState<string>('')
    const { register, control, formState: {errors}, setValue, handleSubmit } = useForm<IFormInputs>();
    const onSubmit: SubmitHandler<IFormInputs> = (data) => {
        addCard(data);
    };
    const addCard = async (data:ICard) => {
        try {
            setLoading(true);
            const res = await fetch('http://localhost:4200/cards', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
            });
            const json = await res.json();
            if (!json) {
                throw Error('fetch failed');
            }
            router.push('/cards')
        } catch (e) {
            console.error(e);
            handleError( 'Something went wrong');
        } finally {
            setLoading(false)
        }
    }
    const onBlurNumber = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value.replace(/[\s_]+/g, '');
        const cartType = regExpArr.find(reg => reg.value.test(value));
        setValue('type', cartType ? cartType.key : '');
    }
    const renderInputNumber = ({field}: {field: ControllerRenderProps<IFormInputs, 'number'>}): ReactElement => {
        return (
            <>
                <InputMask mask="9999 9999 9999 9999" value={field.value} onChange={field.onChange} onBlur={onBlurNumber}>
                    {(inputProps: IMaskProps) => (
                        <input
                            {...inputProps}
                            className={`form-control ${errors.number && 'is-invalid'}`}
                        />
                    )}
                </InputMask>
                {errors.number && <div className="invalid-feedback">{errors.number.message}</div>}
            </>
        )
    }
    const renderInputDate = ({field}: {field: ControllerRenderProps<IFormInputs, 'expiryDate'>}): ReactElement =>(
        <>
            <InputMask mask="99/99" value={field.value} onChange={field.onChange}>
                {(inputProps: IMaskProps) => (
                    <input
                        {...inputProps}
                        className={`form-control ${errors.expiryDate && 'is-invalid'}`}
                    />
                )}
            </InputMask>
            {errors.expiryDate && <div className="invalid-feedback">{errors.expiryDate.message}</div>}
        </>
    )
    return (
        <PageLayout title='Add card'>
            <h2  className='text-center mb-5 mt-5'>Add new credit card</h2>
            <div className="row justify-content-center">
                {error && <div className="alert alert-danger col-md-6 col-sm-8 col-11">{error}</div>}
                <form onSubmit={handleSubmit(onSubmit)} className='form col-md-6 col-sm-8 col-11' noValidate>
                    <label htmlFor="number" className="form-label">Card number</label>
                    <Controller
                        name="number"
                        control={control}
                        rules={{
                            required: validationRules.required(),
                            pattern: validationRules.pattern(/^[0-9\s]+$/, 'invalid card number')
                        }}
                        render={renderInputNumber}
                        defaultValue={""}
                    />
                    <div className='mb-3'>
                        <label htmlFor="owner" className="form-label">Name</label>
                        <input id='owner'
                               {...register("owner", { required: validationRules.required() })}
                               className={`form-control ${errors.owner && 'is-invalid'}`}
                               autoComplete="off"
                        />
                        {errors.owner && <div className="invalid-feedback">{errors.owner.message}</div>}
                    </div>
                    <div className='mb-3'>
                        <label htmlFor="expiryDate" className="form-label">Expiry date</label>
                        <Controller
                            name="expiryDate"
                            control={control}
                            rules={{
                                required: validationRules.required(),
                                pattern: validationRules.pattern(/^(0[1-9]|1[0-2])\/?([0-9]{2})$/, 'invalid date')
                            }}
                            render={renderInputDate}
                            defaultValue={""}
                        />
                    </div>
                    <div className='mb-3'>
                        <label htmlFor="cvv" className="form-label">CVV</label>
                        <input id='cvv'
                               {...register("cvv", {
                                   required: validationRules.required(),
                                   pattern: validationRules.pattern(/^[0-9]{3,4}$/, 'invalid CVV code'),
                               })}
                               className={`form-control ${errors.cvv && 'is-invalid'}`}
                               autoComplete="off"
                               type='number'
                        />
                        {errors.cvv && <div className="invalid-feedback">{errors.cvv.message}</div>}
                    </div>
                    <div className='mb-3'>
                        <label htmlFor="type" className="form-label">Type</label>
                        <input id='type'
                               {...register("type", {
                                   required: validationRules.required(),
                               })}
                               className={`form-control ${errors.type && 'is-invalid'}`}
                               autoComplete="off"
                        />
                        {errors.type && <div className="invalid-feedback">{errors.type.message}</div>}
                    </div>
                    <div className='mb-3'>
                        <label htmlFor="name" className="form-label">Nickname</label>
                        <input id='name'
                               {...register("nickname")}
                               className={`form-control`}
                        />
                    </div>
                    <div className='mb-3'>
                        <button className='btn btn-primary' disabled={loading}>{loading ? 'Adding' : 'Add' }</button>
                    </div>
                </form>
            </div>
        </PageLayout>
    )
}

export default AddCardPage;
